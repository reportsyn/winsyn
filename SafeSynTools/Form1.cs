﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int win_count = Convert.ToInt32(ConfigurationManager.AppSettings["win_count"]);//获取实时同步的筆數
        int win_time = Convert.ToInt32(ConfigurationManager.AppSettings["win_time"]);//获取实时同步的执行间隔时间
        int win_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["win_waitX"]);//获取实时同步的执行间隔倍數
        string moveIdentityid = ConfigurationManager.AppSettings["moveIdentityid"].ToString();
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string mainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_main"].ToString();//main
        string sql_agentOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_agentOut"].ToString();
        string sql_totalIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalIn"].ToString();
        //string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        bool reportDelay = false;

        public Form1()
        {
            InitializeComponent();
            win_time = win_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(syn_dt_lottery_winning_record));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();
        }

        #region 实时同步的方法

        void syn_dt_lottery_winning_record()
        {
            FillMsg1("正在同步...");
            string tablename = ConfigurationManager.AppSettings["onlyOne_win"].ToString();
            int count = 0;
            int sleep_time = win_time;
            string idStr = "";
            //無窮迴圈
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }

                count = 0;
                try
                {
                    //List<SqlParameter> LocalSqlParamter = new List<SqlParameter>(){new SqlParameter("@tableName",tablename)};
                    //string querystr = "select lockid from dt_tablelockid where tableName=@tablename";
                    //var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_agentOut);
                    //byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    //List<string> maxLockid_list = new List<string>();
                    //List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    //SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    //pa_lockid.Value = lockid;
                    //AzureSqlParamter.Add(pa_lockid);

                    string str = "select top " + win_count + " id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName from " + tablename + " with(index(index_reportSyn)) where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "flog=0 order by id";
                    var table = SqlDbHelper.GetQuery(str, (reportDelay == true ? mainConnStr : reportConnStr));
                    if (table == null)
                        continue;
                    //string str = string.Format("select top " + win_count + " id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName,lockid from " + tablename + " where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "flog=0 order by lockid asc OPTION (MAXDOP 2)");
                    //var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), reportConnStr);
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        idStr = "";
                        List<SqlParameter> SqlParamter = new List<SqlParameter>();
                        //for (int i = table.Rows.Count - 1; i >= 0; i--)
                        //{
                        //    byte[] a = (byte[])table.Rows[i]["lockid"];
                        //    string lockid_list = BitConverter.ToString(a).Replace("-", "");
                        //    maxLockid_list.Add(lockid_list);
                        //}
                        //string maxlockid = "0x" + maxLockid_list.Max();
                        //table.Columns.Remove("lockid");

                        for (int r = 0; r < table.Rows.Count; r++)
                        {
                            idStr += "@id" + (r + 1) + ",";
                            SqlParamter.Add(new SqlParameter("@id" + (r + 1), table.Rows[r]["id"].ToString().TrimEnd()));
                        }
                        idStr = idStr.Substring(0, idStr.Length - 1);

                        WriteLogData("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                        TimeSpan tsA1 = new TimeSpan(DateTime.Now.Ticks);
                        int resultA = SqlDbHelper.RunInsert(table, "isVerify", "dsp_lottery_winning_proxySum_self", sql_agentOut);
                        TimeSpan tsA2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan tsA = tsA1.Subtract(tsA2).Duration();
                        string dateDiffA = tsA.Seconds.ToString() + "秒";
                        FillMsg1("Agent 成功汇总" + count + "条,耗时:" + dateDiffA);
                        WriteLogData("Agent 成功汇总" + count + "条数据,耗时:" + dateDiffA + "    同步汇总时间:" + DateTime.Now.ToString());
                        //一樣的資料送到TotalIn
                        TimeSpan tsT1 = new TimeSpan(DateTime.Now.Ticks);
                        int resultT = SqlDbHelper.RunInsert(table, "isVerify", "dsp_lottery_winning_synSum_self", sql_totalIn);
                        TimeSpan tsT2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan tsT = tsT1.Subtract(tsT2).Duration();
                        string dateDiffT = tsT.Seconds.ToString() + "秒";
                        FillMsg1("Total 成功汇总" + count + "条,耗时:" + dateDiffT);
                        WriteLogData("Total 成功汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());

                        table.Columns.Remove("identityid");
                        table.Columns.Remove("user_id");
                        table.Columns.Remove("lottery_code");
                        table.Columns.Remove("bonus_money");
                        table.Columns.Remove("add_time");
                        table.Columns.Remove("SourceName");
                        SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                        Parameter_id.Value = table;
                        Parameter_id.TypeName = "report_id_type";
                        SqlParamter.Add(Parameter_id);

                        if (resultA == 0 && resultT == 0)
                        {
                            WriteLogData("主表 開始更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                            TimeSpan tsU1 = new TimeSpan(DateTime.Now.Ticks);
                            SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                            TimeSpan tsU2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan tsU = tsU1.Subtract(tsU2).Duration();
                            string dateDiffU = tsU.Seconds.ToString() + "秒";
                            WriteLogData("主表 結束更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                            FillMsg1("成功更新主表" + count + "条数据,耗时:" + dateDiffU);

                            //FillMsg1("Agent 成功同步汇总" + count + "条数据,耗时:" + dateDiffA);
                            //WriteLogData("Agent 成功同步汇总" + count + "条数据,耗时:" + dateDiffA + "    同步汇总时间:" + DateTime.Now.ToString());
                            //FillMsg1("Total 成功同步汇总" + count + "条数据,耗时:" + dateDiffT);
                            //WriteLogData("Total 成功同步汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());
                            WriteLogData(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == win_count)
                {
                    sleep_time = 1000;
                }
                else if (count >= win_count / 3 * 2)
                {
                    sleep_time = win_time;
                }
                else if (count < win_count / 3 * 2 && count >= win_count / 3)
                {
                    sleep_time = win_time * win_waitX;
                }
                else if (count < win_count / 3)
                {
                    sleep_time = win_time * (win_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg) 
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else 
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "40")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, mainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport).TotalSeconds >= 60)
                {
                    //delaySecond = Convert.ToInt32(timeMain.Subtract(timeReport).TotalSeconds);
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    //delaySecond=0;
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
